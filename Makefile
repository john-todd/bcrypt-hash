VER=`git tag | tail -1`
COMMIT=`git rev-parse HEAD | cut -b 1-8`
LDFLAGS=-ldflags "-s -w -X main.BuildVer=${VER}.${COMMIT}"

build-macos:
	GOOS=darwin GOARCH=amd64 go build -o bcrypt-hash ${LDFLAGS} bcrypt-hash.go

build-linux:
	GOOS=linux GOARCH=amd64 go build -o bcrypt-hash ${LDFLAGS} bcrypt-hash.go

build-windows:
	GOOS=windows GOARCH=amd64 go build -o bcrypt-hash ${LDFLAGS} bcrypt-hash.go
