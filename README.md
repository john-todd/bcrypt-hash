# bcrypt-hash #

bcrypt-hash is a utiliy to print and match bcrypt hashes of data.

The linux [bcrypt](https://linux.die.net/man/1/bcrypt) utility encrypts files; bcrypt-hash prints and matches bcrypt hash strings.

### Print a bcrypt hash ###

bcrypt-hash outputs the bcrypt hash of stdin data
  
```
printf "hello world" | bcrypt-hash
$2a$10$EeO1td6vYTB08jKhnDQALOhCtz7K.YaizNGL5qtZzzaoE6R2o8Iju
```

### Match input to a bcrypt hash ###

bcrypt-hash prints true / false and returns 0 / 1 when matching stdin data to a hash

```
printf "hello world" | bcrypt-hash -m '$2a$10$EeO1td6vYTB08jKhnDQALOhCtz7K.YaizNGL5qtZzzaoE6R2o8Iju'
true
```

```
printf "hello world\n" | bcrypt-hash -m '$2a$10$EeO1td6vYTB08jKhnDQALOhCtz7K.YaizNGL5qtZzzaoE6R2o8Iju'
false
```

### Use a cost factor in bcrypt-hash

bcrypt-hash accepts a cost factor for cases when that feature is needed. The default cost factor is 10

```
time printf "hello world" | bcrypt-hash -c 12
$2a$12$XRCphURgYYaapcEPk397YOs6bwOjPZt52uW8BbHga6wfpvZ4usfdy

real	0m0.259s
user	0m0.255s
sys	    0m0.004s
```

```
time printf "hello world" | bcrypt-hash -c 20
$2a$20$8XFYP9gB0HGjgGK5IxenZOB6AbFLYxzA8cXbobQn3EuPoGDX1mSPW

real	1m4.481s
user	1m4.398s
sys	    0m0.090s
```

### Print input size and content ###

bcrypt-hash prints the size of the input data with the -v option, and prints the data itself with the -vv option

```
cat /usr/bin/emacs | bcrypt-hash -v
117164432 bytes scanned
$2a$10$CReGwmHe87ACEUd4ta3KtuB91ua3EGHi7fIaGvLuMDmFlr/ttoesC
```

```
printf "\nhello world\ngoodbye world\n" | bcrypt-hash -vv
 
hello world

goodbye world

27 bytes scanned
$2a$10$Mp6tm.qBaagAfcY3/fYUF.dOoDM5qdDyRGK0lX1JiH6XiTQQq45u6
```

### Building from source ###

Build targets are 64 bit executables for Linux, MacOS, and Windows. Prior to these steps, [install Go on your workstation](https://go.dev/doc/install). 

See the Makefile for build targets. These example steps build the executable for a MacOS computer.

```
git clone https://john-todd@bitbucket.org/john-todd/bcrypt-hash.git
cd bcrypt-hash
make build-macos
```

### Contributing ###

* Go Test coming soon
* Test cases welcome
* Pull requests when bugs are found / fixed
* No plans to add features, but requests will be reviewed and considered