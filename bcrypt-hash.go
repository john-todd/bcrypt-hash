package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"golang.org/x/crypto/bcrypt"
)

var BuildVer string

func printHelp() {
	if len(BuildVer) > 0 {
		fmt.Fprintf(os.Stderr, "  bcrypt-hash version %s\n", BuildVer)
		fmt.Fprintf(os.Stderr, "  https://bitbucket.org/john-todd/bcrypt-hash/\n\n")
	}
	fmt.Fprintf(os.Stderr, "  bcrypt-hash outputs the bcrypt hash of stdin data, for example,\n")
	fmt.Fprintf(os.Stderr, "  printf \"hello world\" | bcrypt-hash\n")
	fmt.Fprintf(os.Stderr, "  $2a$10$EeO1td6vYTB08jKhnDQALOhCtz7K.YaizNGL5qtZzzaoE6R2o8Iju\n\n")
	flag.PrintDefaults()
}

func scanMaxBuffer(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	return len(data), data, nil
}

func main() {
	var cost = flag.Int("c", 10, "cost factor, minimum value 4")
	var match = flag.String("m", "", "optional hash to match against input, in bcrypt format with algorithm identifier, cost, salt, and hash")
	var verbose = flag.Bool("v", false, "verbose, print number of bytes scanned")
	var vverbose = flag.Bool("vv", false, "extra verbose, print input and number of bytes scanned")
	var help = flag.Bool("h", false, "display help text and exit")

	flag.Parse()

	if *help {
		printHelp()
		os.Exit(0)
	}

	// check for presence of stdin
	stat, _ := os.Stdin.Stat()
	if flag.NArg() > 0 || (stat.Mode() & os.ModeCharDevice) != 0 {
		printHelp()
		os.Exit(1)
	}

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(scanMaxBuffer)
	var myBytes []byte
	for scanner.Scan() {
		myBytes = append(myBytes, scanner.Bytes()...)
	}

	if *vverbose {
		fmt.Fprintf(os.Stderr, "%s\n", string(myBytes))
	}
	if *vverbose || *verbose {
		fmt.Fprintf(os.Stderr, "%d bytes scanned\n", len(myBytes))
	}

	if len(*match) > 0 {
		err := bcrypt.CompareHashAndPassword([]byte(*match), myBytes)
		if err != nil {
			fmt.Fprintf(os.Stdout, "false\n")
			os.Exit(1)
		} else {
			fmt.Fprintf(os.Stdout, "true\n")
			os.Exit(0)
		}
	} else {
		bc, err := bcrypt.GenerateFromPassword(myBytes, *cost)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err.Error())
			os.Exit(1)
		} else {
			fmt.Fprintf(os.Stdout, "%s\n", string(bc))
			os.Exit(0)
		}
	}
}
